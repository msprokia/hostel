import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddCollegePage } from './add-college.page';

describe('AddCollegePage', () => {
  let component: AddCollegePage;
  let fixture: ComponentFixture<AddCollegePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCollegePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddCollegePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddCollegePageRoutingModule } from './add-college-routing.module';

import { AddCollegePage } from './add-college.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddCollegePageRoutingModule
  ],
  declarations: [AddCollegePage]
})
export class AddCollegePageModule {}

import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-college',
  templateUrl: './add-college.page.html',
  styleUrls: ['./add-college.page.scss'],
})
export class AddCollegePage implements OnInit {
  name: any;
  address: any;

  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
  }

  AddCollege(form: NgForm){
    if(form.value.name === '')
    {
      return;
    }
    this.name = form.value.name;
    this.address = form.value.address;
    this.save();
  }

  save():void {
    let college ={
      name: this.name,
      address: this.address,
    };
     this.modalCtrl.dismiss(college);
  }

  close():void{
    this.modalCtrl.dismiss();
  }

}

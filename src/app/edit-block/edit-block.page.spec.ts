import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditBlockPage } from './edit-block.page';

describe('EditBlockPage', () => {
  let component: EditBlockPage;
  let fixture: ComponentFixture<EditBlockPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBlockPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditBlockPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

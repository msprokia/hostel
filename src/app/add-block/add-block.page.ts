import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-block',
  templateUrl: './add-block.page.html',
  styleUrls: ['./add-block.page.scss'],
})
export class AddBlockPage implements OnInit {
  college:any;
  name:any;
  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
  }

  AddBlock(form: NgForm){
    if(form.value.college === '' || form.value.name === '')
    {
      return;
    }
    this.college = form.value.college;
    this.name = form.value.name;
    this.save();
  }

  save():void {
    let block ={
      college: this.college,
      name: this.name,
    };
     this.modalCtrl.dismiss(block);
  }

  close():void{
    this.modalCtrl.dismiss();
  }
}

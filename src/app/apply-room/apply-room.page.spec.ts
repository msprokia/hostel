import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ApplyRoomPage } from './apply-room.page';

describe('ApplyRoomPage', () => {
  let component: ApplyRoomPage;
  let fixture: ComponentFixture<ApplyRoomPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplyRoomPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ApplyRoomPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

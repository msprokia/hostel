import { Component, OnInit } from '@angular/core';
import { RoomService } from '../room.service';
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-apply-room',
  templateUrl: './apply-room.page.html',
  styleUrls: ['./apply-room.page.scss'],
})
export class ApplyRoomPage implements OnInit {
  updateRoomForm: FormGroup;
  id: any;
  constructor(private RoomService:RoomService,
    private actRoute: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder) {this.id = this.actRoute.snapshot.paramMap.get('id'); }

  ngOnInit() {
    this.getRoomData(this.id);
    this.updateRoomForm = this.fb.group({
      student: ['']
    })
    
  }

  getRoomData(id) {
    this.RoomService.getRoomId(id).subscribe(res => {
      this.updateRoomForm.setValue({
        student: ""
      });
    });
  }

  updateForm() {
    if (!this.updateRoomForm.valid) {
      return false;
    } else {
      this.RoomService.applyRoom(this.id, this.updateRoomForm.value)
        .subscribe((res) => {
          console.log(res)
          this.updateRoomForm.reset();
          this.router.navigate(['../home']);
        })
    }
}
}

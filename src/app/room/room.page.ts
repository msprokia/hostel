import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { RoomService } from '../room.service';
import { AddRoomPage } from '../add-room/add-room.page';

@Component({
  selector: 'app-room',
  templateUrl: './room.page.html',
  styleUrls: ['./room.page.scss'],
})
export class RoomPage implements OnInit {
  rooms: any;
  constructor(public nav: NavController, 
    public RoomService: RoomService, 
    public modalCtrl: ModalController) {
 
  }


  ngOnInit() {
  }

  ionViewDidEnter() {
    this.rooms =[];
    this.RoomService.getRoom().subscribe(rooms =>{
    this.rooms = rooms;
  })
  }

  async addRoom(){
    let modal = await this.modalCtrl.create({component:AddRoomPage});

    modal.onDidDismiss().then(room => {
      room = room.data;
      if(room){
        this.RoomService.createRoom(room).then((room) => {
          this.rooms.push(room);
          const lastIndex = this.rooms.length-1;
        })
      }
    });
     modal.present();
  }

  deleteRoom(room){
    let index = this.rooms.indexOf(room);

    if(index >-1){
      this.rooms.splice(index, 1);
    }
    this.RoomService.deleteRoom(room._id);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditCollegePage } from './edit-college.page';

const routes: Routes = [
  {
    path: '',
    component: EditCollegePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditCollegePageRoutingModule {}

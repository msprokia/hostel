import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplyhomePage } from './applyhome.page';

const routes: Routes = [
  {
    path: '',
    component: ApplyhomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplyhomePageRoutingModule {}

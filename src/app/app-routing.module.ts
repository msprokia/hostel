import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'college',
    loadChildren: () => import('./college/college.module').then( m => m.CollegePageModule)
  },
  {
    path: 'block',
    loadChildren: () => import('./block/block.module').then( m => m.BlockPageModule)
  },
  {
    path: 'room',
    loadChildren: () => import('./room/room.module').then( m => m.RoomPageModule)
  },
  {
    path: 'add-college',
    loadChildren: () => import('./add-college/add-college.module').then( m => m.AddCollegePageModule)
  },
  {
    path: 'add-block',
    loadChildren: () => import('./add-block/add-block.module').then( m => m.AddBlockPageModule)
  },
  {
    path: 'add-room',
    loadChildren: () => import('./add-room/add-room.module').then( m => m.AddRoomPageModule)
  },
  {
    path: 'edit-college/:id',
    loadChildren: () => import('./edit-college/edit-college.module').then( m => m.EditCollegePageModule)
  },
  {
    path: 'edit-block/:id',
    loadChildren: () => import('./edit-block/edit-block.module').then( m => m.EditBlockPageModule)
  },
  {
    path: 'edit-room/:id',
    loadChildren: () => import('./edit-room/edit-room.module').then( m => m.EditRoomPageModule)
  },
  {
    path: 'applyhome',
    loadChildren: () => import('./applyhome/applyhome.module').then( m => m.ApplyhomePageModule)
  },
  {
    path: 'apply-room/:id',
    loadChildren: () => import('./apply-room/apply-room.module').then( m => m.ApplyRoomPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

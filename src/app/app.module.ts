import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';
import { CollegePageModule } from './college/college.module';
import { BlockPageModule } from './block/block.module';
import { RoomPageModule } from './room/room.module';
import { AddCollegePageModule } from './add-college/add-college.module';
import { AddBlockPageModule } from './add-block/add-block.module';
import { AddRoomPageModule } from './add-room/add-room.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, CollegePageModule, BlockPageModule, RoomPageModule, AddCollegePageModule, AddBlockPageModule, AddRoomPageModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

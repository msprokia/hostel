import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BlockService {

  constructor(public http: HttpClient) { }

  getBlockId(id){
    return this.http.get('http://localhost:3000/block/' + id);
  }

  getBlock(){
    return this.http.get('http://localhost:3000/block');
  }

  createBlock(block){
    console.log(block)
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.post('http://localhost:3000/block', block, {headers: headers}).toPromise()
  }

  updateBlock(id, block){
    console.log(block)
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.put('http://localhost:3000/block/' + id, block, {headers: headers})
  }

  deleteBlock(id){
    this.http.delete('http://localhost:3000/block/' + id).subscribe((res:any) =>
    {console.log(res);
  });
}
}

import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { CollegeService } from '../college.service';
import { AddCollegePage } from '../add-college/add-college.page';


@Component({
  selector: 'app-college',
  templateUrl: './college.page.html',
  styleUrls: ['./college.page.scss'],
})
export class CollegePage implements OnInit {
  colleges:any;
  constructor(public nav: NavController, 
    public CollegeService: CollegeService, 
    public modalCtrl: ModalController)  {
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.colleges =[];
    this.CollegeService.getCollege().subscribe(colleges =>{
    this.colleges = colleges;
    })
  }

  async addCollege(){
    let modal = await this.modalCtrl.create({component:AddCollegePage});

    modal.onDidDismiss().then(college => {
      college = college.data;
      if(college){
        this.CollegeService.createCollege(college).then((college) => {
          this.colleges.push(college);
          const lastIndex = this.colleges.length-1;
        })
      }
    });
     modal.present();
  }
  

  deleteCollege(college){
    let index = this.colleges.indexOf(college);

    if(index >-1){
      this.colleges.splice(index, 1);
    }
    this.CollegeService.deleteCollege(college._id);
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }


}
